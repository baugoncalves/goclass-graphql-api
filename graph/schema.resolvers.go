package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"gitlab.com/baugoncalves/goclass-graphql-api/graph/generated"
	"gitlab.com/baugoncalves/goclass-graphql-api/graph/model"
)

func (r *mutationResolver) CreateUser(ctx context.Context, user *model.UserInput) (*bool, error) {
	var userAux model.User

	ok := userAux.NewUser(*user)

	return &ok, nil
}

func (r *mutationResolver) CreateTask(ctx context.Context, task *model.TaskInput) (*bool, error) {
	var taskAux model.Task

	ok := taskAux.NewTask(*task)

	return &ok, nil
}

func (r *mutationResolver) SignIn(ctx context.Context, user *string, password *string) (*model.User, error) {
	var userMod model.User

	ok := userMod.SignIn(*user, *password)

	return &ok, nil
}

func (r *queryResolver) AllUsers(ctx context.Context) ([]*model.User, error) {
	var user model.User

	return user.AllUsers()
}

func (r *queryResolver) AllTasks(ctx context.Context) ([]*model.Task, error) {
	var task model.Task

	return task.AllTasks()
}

func (r *taskResolver) UserRef(ctx context.Context, obj *model.Task) (*model.User, error) {
	var user model.User

	return user.FindUserById(obj.User.ID)
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

// Task returns generated.TaskResolver implementation.
func (r *Resolver) Task() generated.TaskResolver { return &taskResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
type taskResolver struct{ *Resolver }
