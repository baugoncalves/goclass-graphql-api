package model

import (
	"fmt"

	"gitlab.com/baugoncalves/goclass-graphql-api/src/database"
	"gitlab.com/baugoncalves/goclass-graphql-api/src/share"
)

type User struct {
	ID       string `json:"id"`
	Username string `json:"username"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

type UserInput struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

func (u User) AllUsers() ([]*User, error) {
	var users []*User

	conn := database.SetConnection()
	defer conn.Close()

	selDB, err := conn.Query("select *from users")
	if err != nil {
		fmt.Println("Error to fetch", err)
	}
	for selDB.Next() {
		var user = User{}

		err = selDB.Scan(&user.ID, &user.Username, &user.Password, &user.Email)
		users = append(users, &user)
	}

	return users, err
}

func (u User) FindUserById(id string) (*User, error) {
	var user = User{}

	conn := database.SetConnection()
	defer conn.Close()

	selDB, err := conn.Query("select *from users where id=?", id)
	if err != nil {
		fmt.Println("Error to fetch", err)
	}
	for selDB.Next() {

		err = selDB.Scan(&user.ID, &user.Username, &user.Password, &user.Email)
	}

	return &user, err
}

func (u User) NewUser(userInp UserInput) bool {

	conn := database.SetConnection()
	defer conn.Close()

	changedPassword, _ := share.HashPassword(userInp.Password)

	action, _ := conn.Prepare("insert into users (username, password, email) values(?,?,?)")
	action.Exec(userInp.Username, changedPassword, userInp.Email)

	return true
}

func (u User) SignIn(username, password string) User {
	conn := database.SetConnection()
	defer conn.Close()
	var user User
	var datas []interface{}

	datas = append(datas, username)

	selDB, err := conn.Query("select *from users where username=?", datas...)
	if err != nil {
		fmt.Println("Error to fetch", err)
	}
	for selDB.Next() {
		var userAux = User{}

		err = selDB.Scan(&userAux.ID, &userAux.Username, &userAux.Password, &userAux.Email)

		if share.CheckPasswordHash(password, userAux.Password) {
			user = userAux
		}
	}

	return user

}
