package model

import (
	"fmt"

	"gitlab.com/baugoncalves/goclass-graphql-api/src/database"
)

type Task struct {
	ID          string `json:"id"`
	Description string `json:"description"`
	Done        bool   `json:"done"`
	User        User   `json:"user"`
}

type TaskInput struct {
	Description string `json:"description"`
	Done        bool   `json:"done"`
	User        string `json:"user"`
}

func (t Task) AllTasks() ([]*Task, error) {
	var tasks []*Task

	conn := database.SetConnection()
	defer conn.Close()

	selDB, err := conn.Query("select *from tasks")
	if err != nil {
		fmt.Println("Error to fetch", err)
	}
	for selDB.Next() {
		var task = Task{}
		var user = User{}

		err = selDB.Scan(&task.ID, &task.Description, &task.Done, &user.ID)
		task.User = user
		tasks = append(tasks, &task)
	}

	return tasks, err
}

func (t Task) NewTask(taskInp TaskInput) bool {
	conn := database.SetConnection()
	defer conn.Close()

	action, _ := conn.Prepare("insert into tasks (description, done, fk_user) values(?,?,?)")
	action.Exec(taskInp.Description, taskInp.Done, taskInp.User)

	return true
}
