module gitlab.com/baugoncalves/goclass-graphql-api

go 1.15

require (
	github.com/99designs/gqlgen v0.13.0
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/go-sql-driver/mysql v1.5.0
	github.com/vektah/gqlparser/v2 v2.1.0
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
)
